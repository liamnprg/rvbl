#![no_std]

#![feature(asm)]
#![crate_type = "staticlib"]

use core::ptr;
use riscv::interrupt;
use riscv::register;
use core::panic::PanicInfo;
use core::fmt;
use lazy_static::lazy_static;
use spin::Mutex;

lazy_static! {
    pub static ref WRITER: Mutex<Writer> = Mutex::new(Writer {});
}

#[no_mangle]
pub extern "C" fn kmain() -> ! {
    real_main();
}

fn real_main() -> ! {
    println!("[------Welcome to RVBL!------]");
    loop {}
}

pub fn serial_tx(c: i32) {
	let stx: *mut i32 = 0x10000000 as *mut i32;
	unsafe {
		ptr::write_volatile(stx,c);
	}
}


#[no_mangle]
pub extern "C" fn abort() -> ! {
    serial_tx(85);
    loop{}
}
#[no_mangle]
#[panic_handler]
pub extern "C" fn panic(_:&PanicInfo) -> ! {
    serial_tx(90);
    loop{}
}

#[no_mangle]
pub extern "C" fn recall() {
    
    unsafe { interrupt::disable();
    }
    let b = register::mstatus::read();
    serial_tx(68);
    loop{}
}

pub struct Writer {
}
impl Writer {
    pub fn write_byte(&mut self, c: char) {
        let uart = 0x1000_0000  as *mut u8;
        unsafe {
            *uart = c as u8;
        }
    }

    fn new_line(&mut self) {}
}



impl fmt::Write for Writer {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        for c in s.chars() {
            self.write_byte(c);
        }
        Ok(())
    }
}
#[macro_export]
macro_rules! println {
    () => (print!("\n"));
    ($($arg:tt)*) => (print!("{}\n", format_args!($($arg)*)));
}
#[doc(hidden)]
pub fn _print(args: fmt::Arguments) {
    use core::fmt::Write;
    WRITER.lock().write_fmt(args).unwrap();
}
#[macro_export]
macro_rules! print {
    ($($arg:tt)*) => (_print(format_args!($($arg)*)));
}
