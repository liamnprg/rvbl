all: 
	rm -f asrc/main.o rsrc/kernel.o kernel
	cd asrc && make
	cd rsrc && make
	riscv64-linux-gnu-ld asrc/main.o rsrc/kernel.a -T link.ld -o kernel
r: all
	qemu-system-riscv64 -machine virt -nographic -kernel kernel
d: all
	qemu-system-riscv64 -machine virt -S -s -nographic -kernel kernel
gdb:
	riscv64-linux-gnu-gdb -q kernel
o: all
	riscv64-linux-gnu-objdump -D kernel

clean:
	rm main.o a.out
